#include <iostream>

using namespace std;

int main()
{
    int n;
    cin>>n;

    if(n <= 0)
    {
      cout<<"Please enter a number greater than 0"<<endl;
      return 0;
    }

    int i = 0;
    double sum = 0;
    double number;
    while (i < n)
    {
        cin>>number;
        sum+=number;
        ++i;
    }
    cout<<sum<<endl;

    return 0;
}
