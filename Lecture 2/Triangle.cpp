#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Triangle
{

private:
    double a, b, c;

public:
    Triangle(double sideA, double sideB, double sideC): a(sideA), b(sideB), c(sideC){}

    double get_perimeter()
    {
        return a + b + c;
    }

    double get_area()
    {
        double perimeter = (a+b+c)/2;
        double area = sqrt(perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c));
        return (area);
    }

    bool exists(double a, double b, double c)
    {
        return a + b > c && a + c > b && b + c > a;
    }
};

int main()
{
    double a, b, c;
    cout << "Enter sides a, b, and c: ";
    cin >> a >> b >> c;

    Triangle t(a, b, c);

    if(!t.exists(a, b, c))
    {
        cout << "No such triangle!" << endl;
        return 1;
    }

    cout << "The triangle's perimeter is: " << t.get_perimeter() << endl;
    cout << "The triangle's area is: " << t.get_area() << endl;

    return 0;
}
