#include <iostream>
#include <string>
using namespace std;

class GradeBook
{
public:
    void displayMessage(string courseName)
    {
        cout<<"Welcome to the Grade Book for\n"<< courseName << "!"<<endl;
    }
};
int main()
{
    string name0fCourse;
    GradeBook myGradeBook;
    cout<<"Please enter the course name:"<<endl;
    getline(cin, name0fCourse);
    cout<<endl;

    myGradeBook.displayMessage(name0fCourse);
}
