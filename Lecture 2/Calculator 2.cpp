#include <iostream>
#include <string>
using namespace std;

int main()
{
    char operation;
    double total;
    cout << "Enter operation(a/s/m/p):\n";
    cin >> operation;

    cout << "Enter operand 1:\n";
    double operand1;
    cin >> operand1;

    cout << "Enter operand 2:\n";
    double operand2;
    cin >> operand2;

    switch(operation)
{
    case 'a':
    total = operand1 + operand2;
    cout << operand1 << " + " << operand2 << " = " << total << "\n";
        break;

    case 's':
    total = operand1 - operand2;
    cout << operand1 << " - " << operand2 << " = " << total << "\n";
        break;

    case 'm':
    total = operand1 * operand2;
    cout << operand1 << " * " << operand2 << " = " << total << "\n";
        break;

    case 'p':
    total = operand1 / operand2;
    cout << operand1 << " / " << operand2 << " = " << total << "\n";
        break;
}

    return 0;
}
