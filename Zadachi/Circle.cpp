#include <iostream>
#include <string>

using namespace std;

class Circle{

private:
    double r;
    const double PI = 3.14;

public:
    Circle(double radius): r(radius){}

    double get_perimeter()
    {
        return 2*PI*r;
    }

    double get_area()
    {
        return PI*r*r;
    }
};

int main()
{
    double radius;

    cout << "Enter radius: ";
    cin >> radius;

    Circle c(radius);

    cout << "Circle's perimeter is: " << c.get_perimeter() << endl;
    cout << "Circle's area is: " << c.get_area() << endl;

    return 0;
}
