#include <iostream>
#include <string>
using namespace std;

int main()
{
        string a = "a";
        string s = "s";
        string m = "m";
        string p = "p";
        string operation;
        cout << "Enter operation(a/s/m/p):\n";
        cin >> operation;

        cout << "Enter operand 1:\n";
        double operand1;
        cin >> operand1;

        cout << "Enter operand 2:\n";
        double operand2;
        cin >> operand2;

        if (operation == a)
        {
                double total = operand1 + operand2;
                cout << "Total value = " << total << "\n";

        }
        else if (operation == s)
        {
                double total = operand1 - operand2;
                cout << "Total value = " << total << "\n";

        }
        else if (operation == m)
        {
                double total = operand1 * operand2;
                cout << "Total value = " << total << "\n";

        }
        else if (operation == p)
        {
                double total = operand1 / operand2;
                cout << "Total value = " << total << "\n";

        }
        return 0;
}
